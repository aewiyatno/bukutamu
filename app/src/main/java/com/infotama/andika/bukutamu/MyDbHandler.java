package com.infotama.andika.bukutamu;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Andika on 07/09/2016.
 */
public class MyDbHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bukuTamu.db";
    public static final String TABLE_TAMU = "tamu";

    public static final String COL_ID = "_id";
    public static final String COL_NAMA = "namaTamu";
    public static final String COL_INSTANSI = "instansi";
    public static final String COL_KEPERLUAN = "keperluan";

    public MyDbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_TAMU +"(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_NAMA +
                " VARCHAR(255)," + COL_INSTANSI + " VARCHAR(255), " + COL_KEPERLUAN +" TEXT " + ")";
        db.execSQL(CREATE_PRODUCT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXIST " + TABLE_TAMU);
        onCreate(db);
    }

    public void addTamu(Tamu tamu){
        ContentValues value = new ContentValues();
        value.put(COL_NAMA, tamu.getNama());
        value.put(COL_INSTANSI, tamu.getIntansi());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_TAMU, null, value);

        db.close();
    }

    public Tamu findTamuById(int id){
        String query = "SELECT  * FROM " + TABLE_TAMU + " WHERE " + COL_ID + " = \"" +
                id + "\"";

        Tamu tamu = new Tamu();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query,null);
        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            tamu.set_id(Integer.parseInt(cursor.getString(0)));
            tamu.setNama(cursor.getString(1));
            tamu.setIntansi(cursor.getString(2));
            cursor.close();
        } else {
            tamu = null;
        }
        db.close();
        return tamu;
    }

    public Tamu findProduct(String productname){
        String query = "SELECT  * FROM " + TABLE_TAMU + " WHERE " + COL_ID + " = \"" +
                productname + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        Tamu tamu = new Tamu();

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            tamu.set_id(Integer.parseInt(cursor.getString(0)));
            tamu.setNama(cursor.getString(1));
            tamu.setIntansi(cursor.getString(2));
            cursor.close();
        } else {
            tamu = null;
        }
        db.close();
        return tamu;
    }

    public boolean deleteProduct(String productname){
        boolean result = false;

        String query = "SELECT * FROM " + TABLE_TAMU + " WHERE " + COL_NAMA + " = \"" +
                productname + " \"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        Tamu tamu = new Tamu();

        if(cursor.moveToFirst()){
            tamu.set_id(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_TAMU, COL_ID + " = ?",
                    new String[]{String.valueOf(tamu.get_id())
                    });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }

    public Cursor fetchAllRecord(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_TAMU, new String[]{COL_ID,
                COL_NAMA,COL_INSTANSI,COL_KEPERLUAN},null,null,null,null,null);

        return cursor;
    }
}

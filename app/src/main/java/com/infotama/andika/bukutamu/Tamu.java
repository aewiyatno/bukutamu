package com.infotama.andika.bukutamu;

/**
 * Created by Andika on 07/09/2016.
 */
public class Tamu {
    private int _id;
    private String nama;
    private String intansi;
    private String keperluan;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIntansi() {
        return intansi;
    }

    public void setIntansi(String intansi) {
        this.intansi = intansi;
    }

    public String getKeperluan() {
        return keperluan;
    }

    public void setKeperluan(String keperluan) {
        this.keperluan = keperluan;
    }
}
